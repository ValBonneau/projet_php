<?php
require_once "../Header.php";
require_once "../Footer.php";

myHeader("Accueil");
myFooter();
?>


<?php
$id=filter_input(INPUT_GET,"id");

require_once "../config.php";
$pdo = new PDO("mysql:host=".Config::SERVER.";dbname=".Config::BDD,Config::USER,Config::MDP);
$requete = $pdo->prepare("select nom,description,id
                                   from lot 
                                   where id=:id");
$requete->bindParam(":id",$id);
$requete->execute();
$lignes = $requete->fetchAll();
?>


<form action="../Actions/Supprimer/supprimer_lot.php" method="post" style="margin:5% 20%;">
    <h1> Nom de l'objet: <i> <?php echo htmlspecialchars($lignes[0]["nom"])?> </i> </h1>

    <h3> Description : <i> <?php echo htmlspecialchars($lignes[0]["description"])?> </i> </h3>



    <input type="hidden" value="<?php echo htmlspecialchars($lignes[0]["id"]) ?>" name="id">

    <input type="submit" value="Supprimer" class="btn btn-primary">

</form>