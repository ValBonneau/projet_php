<?php
require_once "../Header.php";
require_once "../Footer.php";

myHeader("Accueil");
myFooter();
?>


<?php
$id=filter_input(INPUT_GET,"id");

require_once "../config.php";
$pdo = new PDO("mysql:host=".Config::SERVER.";dbname=".Config::BDD,Config::USER,Config::MDP);
$requete2 = $pdo->prepare("select l.id,l.id_encheres, l.nom, l.description 
                                   from lot l 
                                   where l.id_encheres=:id");
$requete2->bindParam(":id",$id);
$requete2->execute();
$lot = $requete2->fetchAll();



for ($i = 0; $i < count($lot); $i++) {
    ?>
    <div class="col-3">
        <div class="card">
            <h5 class="card-title"> Nom du lot : <?php echo htmlspecialchars($lot[$i]["nom"])?></h5>

            <div class="card-body">

                <p class="card-text"> Description : <?php echo htmlspecialchars($lot[$i]["description"])?></p>

                <a href="../Objets/ListeObjetsClient.php?id=<?php echo htmlspecialchars($lot[$i][0])?>"  class="btn btn-sm btn-warning">Plus</a>
            </div>
        </div>
    </div>
    <?php
}

?>

<a href="javascript:history.go(-1)" class="btn btn-sm btn-primary" >Retour</a>
