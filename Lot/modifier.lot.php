<?php
require_once "../Header.php";
require_once "../Footer.php";

myHeader("Accueil");
myFooter();
?>


<?php
$id = filter_input(INPUT_GET, "id");
require_once "../config.php";
$pdo = new PDO("mysql:host=" . Config::SERVER . ";dbname=" . Config::BDD, Config::USER, Config::MDP);
$requete = $pdo->prepare("SELECT l.nom,l.description,l.id_encheres,l.id from lot l where id=:id");
$requete->bindParam(":id", $id);
$requete->execute();
$lignes = $requete->fetchAll();

$requete2 = $pdo->prepare("SELECT o.id_lot,o.id,o.nom,o.photo,o.prix_depart,o.prix_reserve,o.description from objet o where o.id_lot=:id");
$requete2->bindParam(":id", $id);
$requete2->execute();
$objet = $requete2->fetchAll();


?>
<form action="../Actions/Modifier/modifier_lot.php" method="post" style="margin:5% 20%;">
    <div class="form-group">
        <label for="Lot">Nom du lot</label>
        <input type="text" value="<?php echo htmlspecialchars($lignes[0]["nom"]) ?>" class="form-control" name="nom"
               aria-describedby="Le nom du lot">
    </div>

    <div class="form-group">
        <label for="Lot">Description</label>
        <input type="text" value="<?php echo htmlspecialchars($lignes[0]["description"]) ?>" class="form-control" name="description"
               aria-describedby="Sa description">
    </div>
    <input type="hidden" value="<?php echo $lignes[0]["id"] ?>" name="id">

    <input type="submit" value="Modifier" class="btn btn-primary">

</form>


<?php

for ($i = 0; $i < count($objet); $i++) {
    ?>
    <div class="col-3">
        <div class="card">
            <img src="<?php echo $objet[$i]["photo"] ?>" alt="" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title"> Nom de l'objet: <?php echo htmlspecialchars($objet[$i]["nom"]) ?></h5>
                <p class="card-text"> Description : <?php echo htmlspecialchars($objet[$i]["description"]) ?></p>
                <p class="card-text"> Prix de départ : <?php echo htmlspecialchars($objet[$i]["prix_depart"]) ?></p>
                <p class="card-text"> Prix de réserve : <?php echo htmlspecialchars($objet[$i]["prix_reserve"]) ?></p>
                <a href="../Objets/modifier.objet.php?id=<?php echo htmlspecialchars($objet[$i]["id"]) ?>"
                   class="btn btn-sm btn-warning">Modifier</a>
                <a href="../Objets/supprimer.objet.php?id=<?php echo htmlspecialchars($objet[$i]["id"]) ?>"
                   class="btn btn-sm btn-warning">Supprimer</a>
            </div>
        </div>
    </div>
    <?php
}

?>
<a href="../Objets/ajouter.objet.php?id=<?php echo htmlspecialchars($id) ?>" class="btn btn-sm btn-primary">Ajouter un
    objet</a>
<a href="javascript:history.go(-1)" class="btn btn-sm btn-primary" >Retour</a>


