<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Ajouter Lot - BidGameCoin</title>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <?php
    require_once "../config.php";
    $id=filter_input(INPUT_GET,"id");


    ?>



</head>
<body>
<form action="../Actions/Ajouter/ajouter_objet.php" method="post" style="margin:5% 20%;">
    <div class="form-group">
        <label for="Objet">Nom de l'objet</label>
        <input type="text" maxlength="20"  class="form-control" name="nom" aria-describedby="Le nom de l'objet">
    </div>

    <div class="form-group">
        <label for="Objet">Description</label>
        <input type="text" maxlength="255" class="form-control" name="description" aria-describedby="Sa description">
    </div>

    <div class="form-group">
        <label for="Objet">Photo de l'objet</label>
        <input type="url" class="form-control" name="photo" aria-describedby="La photo de l'objet">
    </div>

    <div class="form-group">
        <label for="Objet">Prix de départ</label>
        <input type="float"  class="form-control" name="prix_depart" aria-describedby="Le prix de départ">
    </div>

    <div class="form-group">
        <label for="Objet">Prix de reserve</label>
        <input type="float" maxlength="20"  class="form-control" name="prix_reserve" aria-describedby="Le prix de reserve">
    </div>

    <input type="hidden" value="<?php echo htmlspecialchars($id) ?>" name="id_lot">



    <input type="submit" class="btn btn-primary">

</form>

</body>

