<?php
require_once "../Header.php";
require_once "../Footer.php";

myHeader("Accueil");
myFooter();
?>


<?php
$id = filter_input(INPUT_GET, "id");
require_once "../config.php";
$pdo = new PDO("mysql:host=" . Config::SERVER . ";dbname=" . Config::BDD, Config::USER, Config::MDP);
$requete = $pdo->prepare("SELECT o.photo,o.prix_depart,o.prix_reserve,o.nom,o.description,o.id from objet o  where o.id=:id");
$requete->bindParam(":id", $id);
$requete->execute();
$lignes = $requete->fetchAll();


?>
<form action="../Actions/Modifier/modifier_objet.php" method="post" style="margin:5% 20%;">
    <div class="form-group">
        <label for="Objet">Nom du lot</label>
        <input type="text" value="<?php echo htmlspecialchars($lignes[0]["nom"]) ?>" class="form-control" name="nom"
               aria-describedby="Le nom du lot">
    </div>

    <div class="form-group">
        <label for="Objet">Description</label>
        <input type="text" value="<?php echo htmlspecialchars($lignes[0]["description"]) ?>" class="form-control" name="description"
               aria-describedby="Sa description">
    </div>
    <div class="form-group">
        <label for="Objet">Photo</label>
        <input type="url" value="<?php echo htmlspecialchars($lignes[0]["photo"]) ?>" class="form-control" name="photo"
               aria-describedby="Sa photo">
    </div>
    <div class="form-group">
        <label for="Objet">Prix de départ</label>
        <input type="text" value="<?php echo htmlspecialchars($lignes[0]["prix_depart"]) ?>" class="form-control" name="prix_depart"
               aria-describedby="Son prix">
    </div>
    <div class="form-group">
        <label for="Objet">Prix de reserve</label>
        <input type="text" value="<?php echo htmlspecialchars($lignes[0]["prix_reserve"]) ?>" class="form-control" name="prix_reserve"
               aria-describedby="Son prix de reserve">
    </div>
    <input type="hidden" value="<?php echo $lignes[0]["id"] ?>" name="id">

    <input type="submit" value="Modifier" class="btn btn-primary">

</form>

<a href="javascript:history.go(-1)" class="btn btn-sm btn-primary" >Retour</a>