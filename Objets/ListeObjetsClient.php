<?php
require_once "../Header.php";
require_once "../Footer.php";

myHeader("Accueil");

?>


<?php
$id = filter_input(INPUT_GET, "id");
require_once "../config.php";
$pdo = new PDO("mysql:host=" . Config::SERVER . ";dbname=" . Config::BDD, Config::USER, Config::MDP);
$requete2 = $pdo->prepare("SELECT o.id_lot,o.id,o.nom,o.photo,o.prix_depart,o.description from objet o where o.id_lot=:id");
$requete2->bindParam(":id", $id);
$requete2->execute();
$objet = $requete2->fetchAll();


for ($i = 0; $i < count($objet); $i++) {
    ?>
    <div class="col-3">
        <div class="card">

            <img src="<?php echo $objet[$i]["photo"] ?>" alt="" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title"> Nom : <?php echo htmlspecialchars($objet[$i]["nom"]) ?></h5>
                <p class="card-text"> Description : <?php echo htmlspecialchars($objet[$i]["description"]) ?></p>
                <p class="card-text"> Prix de départ : <?php echo htmlspecialchars($objet[$i]["prix_depart"]) ?></p>
            </div>
        </div>
    </div>
    <?php
}

?>

<a href="javascript:history.go(-1)" class="btn btn-sm btn-primary" >Retour</a>
myFooter();

