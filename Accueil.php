
<?php
require_once "Header.php";
require_once "Footer.php";

myHeader("Accueil");

?>


<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="d-block w-100" src="https://www.notretemps.com/cache/com_zoo_images/ac/chatons-psycho_61d46a6e8d925ecf4a5a25c62360bea8.jpg" height="400px" alt="First slide">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="https://www.notretemps.com/cache/com_zoo_images/ac/chatons-psycho_61d46a6e8d925ecf4a5a25c62360bea8.jpg" height="400px" alt="Second slide">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="https://www.notretemps.com/cache/com_zoo_images/ac/chatons-psycho_61d46a6e8d925ecf4a5a25c62360bea8.jpg" height="400px" alt="Third slide">
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<br><br><br><br><br><br><br><br><br><br>
<?php

myFooter();