<?php
require_once "../Header.php";
require_once "../Footer.php";

myHeader("Accueil");
myFooter();
?>


<?php
$id=filter_input(INPUT_GET,"id");

require_once "../config.php";
$pdo = new PDO("mysql:host=".Config::SERVER.";dbname=".Config::BDD,Config::USER,Config::MDP);
$requete = $pdo->prepare("select e.nom,e.description,e.dateDebut,e.dateFin,e.id
                                   from encheres e 
                                   where e.id=:id");
$requete->bindParam(":id",$id);
$requete->execute();
$lignes = $requete->fetchAll();

$requete2 = $pdo->prepare("select l.id,l.id_encheres, l.nom, l.description 
                                   from lot l 
                                   where l.id_encheres=:id");
$requete2->bindParam(":id",$id);
$requete2->execute();
$lot = $requete2->fetchAll();


?>
<form action="../Actions/Modifier/modifier_enchère.php" method="post" style="margin:5% 20%;">
    <div class="form-group">
        <label for="Enchère">Nom de l'enchère</label>
        <input type="text" value="<?php echo htmlspecialchars($lignes[0]["nom"])?>" class="form-control" name="nom" aria-describedby="Le nom de l'enchère">
    </div>

    <div class="form-group">
        <label for="Enchère">Description</label>
        <input type="text" value="<?php echo htmlspecialchars($lignes[0]["description"])?>" class="form-control" name="description" aria-describedby="Sa description">
    </div>

    <div class="form-group">
        <label for="Enchère">Date de début </label>
        <input type="date" value="<?php echo htmlspecialchars($lignes[0]["dateDebut"])?>" class="form-control" name="dateDebut">

    </div>
    <div class="form-group">
        <label for="Enchère">Date de fin </label>
        <input type="date" value="<?php echo htmlspecialchars($lignes[0]["dateFin"])?>" class="form-control" name="dateFin">

    </div>
    <input type="hidden" value="<?php echo $lignes[0]["id"] ?>" name="id">

    <input type="submit" value="Modifier" class="btn btn-primary">

</form>


<?php






for ($i = 0; $i < count($lot); $i++) {
    ?>
    <div class="col-3">
        <div class="card">
                <h5 class="card-title"> Nom de du lot : <?php echo htmlspecialchars($lot[$i]["nom"])?></h5>
            <div class="card-body">

                <p class="card-text"> Description : <?php echo htmlspecialchars($lot[$i]["description"])?></p>
                <a href="../Lot/modifier.lot.php?id=<?php echo htmlspecialchars($lot[$i][0])?>"  class="btn btn-sm btn-warning">Modifier</a>
                <a href="../Lot/supprimer.lot.php?id=<?php echo htmlspecialchars($lot[$i][0])?>"  class="btn btn-sm btn-warning">Supprimer</a>
            </div>
        </div>
    </div>
    <?php
}

?>
<a href="../Lot/ajouter.lot.php?id=<?php echo htmlspecialchars($id)?>"  class="btn btn-sm btn-primary">Ajouter un lot</a>

<a href="javascript:history.go(-1)" class="btn btn-sm btn-primary" >Retour</a>
