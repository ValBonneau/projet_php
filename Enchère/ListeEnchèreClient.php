<?php
require_once "../Header.php";
require_once "../Footer.php";

myHeader("Accueil");
myFooter();
?>



<?php
require_once "../config.php";
$pdo = new PDO("mysql:host=".Config::SERVER.";dbname=".Config::BDD,Config::USER,Config::MDP);
$requete = $pdo->prepare("select id,dateDebut,dateFin, nom, description from encheres");
$requete->execute();
$lignes = $requete->fetchAll();


for ($i = 0; $i < count($lignes); $i++) {
    ?>
    <div class="col-3">
        <div class="card">
                <h5 class="card-title"> Nom de l'enchère : <?php echo htmlspecialchars($lignes[$i]["nom"])?></h5>
            <div class="card-body">
                <p class="card-text"> Date de début : <?php echo htmlspecialchars($lignes[$i]["dateDebut"])?></p>
                <p class="card-text"> Date de fin : <?php echo htmlspecialchars($lignes[$i]["dateFin"])?></p>
                <p class="card-text">Description : <?php echo htmlspecialchars($lignes[$i]["description"])?></p>
                <a href="../Lot/ListeLotClient.php?id=<?php echo htmlspecialchars($lignes[$i]["id"])?>"  class="btn btn-sm btn-warning">Plus</a>
            </div>
        </div>
    </div>
    <?php
}

?>
<a href="javascript:history.go(-1)" class="btn btn-sm btn-primary" >Retour</a>




