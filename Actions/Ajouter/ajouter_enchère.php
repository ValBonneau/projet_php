<?php
$nom = filter_input(INPUT_POST,"nom");
$description = filter_input(INPUT_POST,"description");
$dateDebut = date('Y-m-d',strtotime( filter_input(INPUT_POST,"dateDebut")));
$dateFin = date('Y-m-d',strtotime( filter_input(INPUT_POST,"dateFin")));



require_once "../../config.php";
$pdo = new PDO("mysql:host=".Config::SERVER.";dbname=".Config::BDD,Config::USER,Config::MDP);

$requete = $pdo->prepare("INSERT INTO encheres (nom,description,dateDebut,dateFin) values (:nom,:description,:dateDebut,:dateFin)");
$requete->bindParam(":nom", $nom);
$requete->bindParam(":description", $description);
$requete->bindParam(":dateDebut", $dateDebut);
$requete->bindParam(":dateFin", $dateFin);
$requete->execute();
$error = $requete->errorInfo();
echo "<br>";
var_dump($error) ;

header("location:../../Enchère/ListeEnchèreAdmin.php");
