<?php
$nom = filter_input(INPUT_POST,"nom");
$description = filter_input(INPUT_POST,"description");
$photo = filter_input(INPUT_POST,"photo");
$prix_reserve = filter_input(INPUT_POST,"prix_reserve");
$prix_depart = filter_input(INPUT_POST,"prix_depart");
$id_lot = filter_input(INPUT_POST,"id_lot");




require_once "../../config.php";
$pdo = new PDO("mysql:host=".Config::SERVER.";dbname=".Config::BDD,Config::USER,Config::MDP);

$requete = $pdo->prepare("INSERT INTO objet (nom,description,photo,prix_reserve,prix_depart,id_lot) values (:nom,:description,:photo,:prix_reserve,:prix_depart,:id_lot)");
$requete->bindParam(":nom", $nom);
$requete->bindParam(":description", $description);
$requete->bindParam(":photo", $photo);
$requete->bindParam(":prix_depart", $prix_depart);
$requete->bindParam(":prix_reserve", $prix_reserve);
$requete->bindParam(":id_lot", $id_lot);

$requete->execute();
$error = $requete->errorInfo();
echo "<br>";
var_dump($error) ;

header("location:../../Lot/modifier.lot.php?id=".$id_lot);
