<?php
$id=filter_input(INPUT_POST,"id");

$nom=filter_input(INPUT_POST,"nom");
$description=filter_input(INPUT_POST,"description");

require_once "../../config.php";
$pdo = new PDO("mysql:host=".Config::SERVER.";dbname=".Config::BDD, Config::USER, Config::MDP);
$requete =  $pdo->prepare("update lot set nom=:nom, description=:description where id=:id");
$requete->bindParam(":id",$id);

$requete->bindParam(":description",$description);
$requete->bindParam(":nom",$nom);
$requete->execute();

header("location:../../Lot/modifier.lot.php?id=$id");