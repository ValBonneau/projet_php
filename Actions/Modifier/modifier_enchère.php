<?php

$id=filter_input(INPUT_POST, "id");
$nom=filter_input(INPUT_POST, "nom");
$description=filter_input(INPUT_POST, "description");
$dateDebut = date('Y-m-d',strtotime( filter_input(INPUT_POST,"dateDebut")));
$dateFin = date('Y-m-d',strtotime( filter_input(INPUT_POST,"dateFin")));



require_once "../../config.php";
$pdo = new PDO("mysql:host=".Config::SERVER.";dbname=".Config::BDD, Config::USER, Config::MDP);


$requete = $pdo->prepare("update encheres set nom=:nom, description=:description, dateDebut=:dateDebut, dateFin=:dateFin where id=:id");
$requete->bindParam(":id", $id);
$requete->bindParam(":nom", $nom);
$requete->bindParam(":description", $description);
$requete->bindParam(":dateDebut", $dateDebut);
$requete->bindParam(":dateFin", $dateFin);


$requete->execute();


header("location:../../Enchère/modifier.enchère.php?id=$id");
