<?php
$id=filter_input(INPUT_POST,"id");
$nom=filter_input(INPUT_POST,"nom");
$description=filter_input(INPUT_POST,"description");
$photo=filter_input(INPUT_POST,"photo");
$prix_depart=filter_input(INPUT_POST,"prix_depart");
$prix_reserve=filter_input(INPUT_POST,"prix_reserve");

require_once "../../config.php";
$pdo = new PDO("mysql:host=".Config::SERVER.";dbname=".Config::BDD, Config::USER, Config::MDP);
$requete =  $pdo->prepare("update objet set nom=:nom, description=:description,photo=:photo,prix_depart=:prix_depart,prix_reserve=:prix_reserve where id=:id");
$requete->bindParam(":id",$id);
$requete->bindParam(":description",$description);
$requete->bindParam(":nom",$nom);
$requete->bindParam(":photo",$photo);
$requete->bindParam(":prix_depart",$prix_depart);
$requete->bindParam(":prix_reserve",$prix_reserve);
$requete->execute();

header("location:../../Objets/modifier.objet.php?id=$id");