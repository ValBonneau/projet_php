<?php
$mail = filter_input(INPUT_POST,"mail");
$password = filter_input(INPUT_POST,"password");

require_once "../config.php";
$pdo = new PDO("mysql:host=".Config::SERVER.";dbname=".Config::BDD,Config::USER,Config::MDP);

$requete = $pdo->prepare('SELECT id,password from user where mail=:mail');
$requete->bindParam(":mail",$mail);
$requete->execute();
$acount = $requete->fetchAll();


if(count($acount)!=0){
    if(password_verify($password, $acount[0]['password'])){
        session_start();
        $_SESSION["idAcount"]= $acount[0]["id"];

        header("location:../Accueil.php");
    }else{
        header("location:../Connexion.php?error=1");
    }
}
else{
    header("location:../Connexion.php?error=2");
    /*error
     * 1 : mauvais password
     * 2 : adresse mail inconue
     *
     *
     *
     */
}
/*
var_dump($acount[0]["password"]);
var_dump(password_hash($password,PASSWORD_DEFAULT));
echo "<br>";
var_dump($password);
*/