<?php
require_once "Header.php";
require_once "Footer.php";

myHeader("Historique d'enchere");

echo "<h1>Historique d'enchere</h1>";

if(array_key_exists("idAcount",$_SESSION) && $_SESSION["idAcount"] != null){
    require_once "config.php";
    $pdo = new PDO("mysql:host=" . Config::SERVER . ";dbname=" . Config::BDD, Config::USER, Config::MDP);

    $requeteObjetV = $pdo->prepare("SELECT id_objet,prix FROM objetv WHERE id_user=:id");
    $requeteObjetV->bindParam(":id",$_SESSION["idAcount"]);
    $requeteObjetV->execute();
    $objetV = $requeteObjetV->fetchAll();
    for ($i=0; $i<count($objetV);$i++){
        $requete2 = $pdo->prepare("SELECT nom,photo,prix_depart,description from objet where id=:id");
        $requete2->bindParam(":id",$objetV[$i]["id_objet"]);
        $requete2->execute();
        $objet = $requete2->fetchAll();

        ?>
        <div class="col-3">
            <div class="card">

                <img src="<?php echo $objet[0]["photo"] ?>" alt="" class="card-img-top">
                <div class="card-body">
                    <h5 class="card-title"> Nom : <?php echo htmlspecialchars($objet[0]["nom"]) ?></h5>
                    <p class="card-text"> Description : <?php echo htmlspecialchars($objet[0]["description"]) ?></p>
                    <p class="card-text"> Prix de départ : <?php echo htmlspecialchars($objet[0]["prix_depart"]) ?></p>
                    <p class="card-text">Votre prix : <?php echo htmlspecialchars($objetV[$i]["prix"]) ?></p>
                </div>
            </div>
        </div>
        <?php
    }
}else{
    echo "Connectez Vous SVP";
}
myFooter();