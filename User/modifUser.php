<?php

$id       = filter_input(INPUT_POST, "id");
$pseudo   = filter_input(INPUT_POST, "pseudo");
$nom      = filter_input(INPUT_POST, "nom");
$prenom   = filter_input(INPUT_POST, "prenom");
$mail     = filter_input(INPUT_POST, "mail");
$dateN    = date('Y-m-d',strtotime( filter_input(INPUT_POST,"dateN")));
$adresseP = filter_input(INPUT_POST, "adresseP");
$Admin = filter_input(INPUT_POST, "Admin");

if($Admin == "on"){
    $isAdmin = 1;
}else{
    $isAdmin = 0;
}
var_dump($mail);
echo "<br><br>";

require_once "../config.php";
$pdo = new PDO("mysql:host=".Config::SERVER.";dbname=".Config::BDD,Config::USER,Config::MDP);

$requette = $pdo->prepare("UPDATE user 
                                    SET pseudo = :pseudo, nom = :nom, prenom = :prenom, dateN = :dateN, adresseP = :adresseP, isAdmin = :isAdmin 
                                    WHERE id = :id and mail = :mail");
$requette->bindParam(":pseudo",$pseudo);
$requette->bindParam(":nom",$nom);
$requette->bindParam(":prenom",$prenom);
$requette->bindParam(":dateN",$dateN);
$requette->bindParam(":adresseP",$adresseP);
$requette->bindParam(":isAdmin",$isAdmin);
$requette->bindParam(":id",$id);
$requette->bindParam(":mail",$mail);
$requette->execute();
$requette->debugDumpParams();
echo "<br><br>";

header("location: userList.php");