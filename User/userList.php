<?php
require_once "../Header.php";
require_once "../Footer.php";

myHeader("Liste des utilisateurs");

require_once "../config.php";
$pdo = new PDO("mysql:host=".Config::SERVER.";dbname=".Config::BDD,Config::USER,Config::MDP);
$requete = $pdo->prepare("SELECT id,pseudo,mail,nom,prenom,dateN,adresseP,isAdmin FROM user");
$requete->execute();
$lignes = $requete->fetchAll();

if(array_key_exists("idAcount",$_SESSION)){
    if($_SESSION["idAcount"] != null){

        $requete=$pdo->prepare("SELECT pseudo,isAdmin FROM user where id=:id");
        $requete->bindParam(":id",$_SESSION["idAcount"]);
        $requete->execute();
        $acount = $requete->fetchAll();
        if($acount[0]["isAdmin"]==1){

            ?>
            <h1>Liste des utilisateurs</h1>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">id</th>
                    <th scope="col">Pseudo</th>
                    <th scope="col">Nom</th>
                    <th scope="col">Prenom</th>
                    <th scope="col">Email</th>
                    <th scope="col">Date de Naissance</th>
                    <th scope="col">Adresse</th>
                    <th scope="col">Admin</th>
                    <th scope="col">Modifier</th>
                </tr>
                </thead>
                <tbody>
                <?php
                for ($i = 0; $i < count($lignes); $i++){
                    ?>
                    <tr><form action="modifUser.php" method="post">
                            <th scope="row"><input disabled type="number" value="<?php echo htmlspecialchars($lignes[$i]["id"])?>" required></th>
                            <input name="id" type="hidden" value="<?php echo htmlspecialchars($lignes[$i]["id"])?>" required>
                            <td><input name="pseudo" type="text" value="<?php echo htmlspecialchars($lignes[$i]["pseudo"]) ?>" required ></td>
                            <td><input name="nom" type="text"value="<?php echo htmlspecialchars($lignes[$i]["nom"]) ?>" required></td>
                            <td><input name="prenom" type="text" value="<?php echo htmlspecialchars($lignes[$i]["prenom"]) ?>" required></td>
                            <td><input disabled type="email" value="<?php echo htmlspecialchars($lignes[$i]["mail"]) ?>" required>
                                <input name="mail" type="hidden" value="<?php echo htmlspecialchars($lignes[$i]["mail"]) ?>" required>
                            </td>
                            <td><input name="dateN" type="date" value="<?php echo htmlspecialchars($lignes[$i]["dateN"]) ?>" required></td>
                            <td><input name="adresseP" type="text" value="<?php echo htmlspecialchars($lignes[$i]["adresseP"]) ?>" required></td>
                            <td>
                                <?php
                                if($lignes[$i]["isAdmin"]==1){
                                    echo '<input name="Admin" class="form-check-input" type="checkbox" checked>' ;
                                }
                                else{
                                    echo '<input name="Admin" class="form-check-input" type="checkbox">' ;
                                }
                                ?>
                            </td>
                            <td>
                                <input type="submit" class="btn btn-warning" value="Modifier">
                            </td>
                    </tr>
                    </form>
                    <?php
                }
                ?>
                </tbody>
            </table>
            <?php
        }
        else{
            echo "tu n'as pas le droit d'être là toi";

        }

    }else{
        echo "Connectez-vous SVP";
    }
}else {
    echo "Connectez-vous SVP";
}
myFooter();