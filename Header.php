<?php
function myHeader($title){
    ?>
    <!doctype html>
    <html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title><?php echo $title?> - BidGameCoin</title>

        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
         <script src="https://kit.fontawesome.com/edc8d5fc95.js" crossorigin="anonymous"></script>
        <script src="../../projet_php/Addons/jquery-3.5.1.min.js"></script>
        <script src="../../projet_php/Addons/p5.min.js"></script>
    </head>
    <body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#"> icon </a>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="../../projet_php/Accueil.php">Accueil<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item dropdown">
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="../../projet_php/Enchère/ListeEnchèreClient.php">Toutes les encheres</a>
                        <hr class="dropdown-divider">
                        <?php
                        require_once "config.php";
                        $pdo = new PDO("mysql:host=".config::SERVER.";dbname=".config::BDD,config::USER,config::MDP);
                        $requete = $pdo->prepare("SELECT nom,id FROM encheres");
                        $requete->execute();
                        $lignes = $requete->fetchAll();


                        for($i=0;$i < count($lignes); $i++){?>
                            <a class="dropdown-item" href="../../projet_php/Lot/ListeLotClient.php?id=<?php echo htmlspecialchars($lignes[$i]["id"]) ?>"><?php echo htmlspecialchars($lignes[$i]["nom"]) ?></a>
                            <?php
                        }

                        ?>
                    </div>
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Enchères
                    </a>

                </li>
                <?php
                    session_start();
                    if(array_key_exists("idAcount",$_SESSION)){
                        if($_SESSION["idAcount"] != null){

                            $requete=$pdo->prepare("SELECT pseudo,isAdmin FROM user where id=:id");
                            $requete->bindParam(":id",$_SESSION["idAcount"]);
                            $requete->execute();
                            $acount = $requete->fetchAll();
                            if($acount[0]["isAdmin"]==1){
                                echo '<a class="nav-link" href="../../projet_php/Enchère/ListeEnchèreAdmin.php">Administration des encheres</a>';
                                echo '<a class="nav-link" href="../../projet_php/User/userList.php">Listes des utilisateurs</a>';
                                echo '<a class="nav-link" href="../../projet_php/Salle/SalleEnchereAdmin.php">Salle des encheres</a>';
                            }
                            else{
                                echo '<a class="nav-link" href="../../projet_php/Salle/SalleEnchere.php">Salle des encheres</a>';
                                echo '<a class="nav-link" href="../../projet_php/Historique.php">Historique d&apos;achat</a>';
                            }
                            echo '<li class="nav-item"><a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Connecté en temps que '.$acount[0]["pseudo"].'</a></li>';
                            echo '<a class="btn btn-warning" href="../../projet_php/InformationUser.php" role="button">Mon Compte</a>';
                            echo '<a class="btn btn-warning" href="../../projet_php/actions/deconnexion.php" role="button">Deconnexion</a>';


                        }
                    }
                    else{
                        ?>
                        <a class="btn btn-warning" href="../../projet_php/Register.php" role="button">Inscritpion</a>
                        <a class="btn btn-warning" href="../../projet_php/Connexion.php" role="button">Connexion</a>
                        <?php
                    }
                ?>
            </ul>
        </div>
    </nav>


<?php
}
?>


