<?php
require_once "../config.php";
$pdo = new PDO("mysql:host=".config::SERVER.";dbname=".config::BDD,config::USER,config::MDP);

$requeteEnchere = $pdo->prepare("SELECT id,id_objet FROM encheresencours");
$requeteEnchere->execute();
$enchere = $requeteEnchere->fetchAll();

$requeteMaxPrix=$pdo->prepare("SELECT MAX(prix) FROM prixutilisateurencours WHERE id_encheres_encours=:id");
$requeteMaxPrix->bindParam(":id",$enchere[0]["id"]);
$requeteMaxPrix->execute();
$MaxPrix = $requeteMaxPrix->fetchAll();

$requete2 = $pdo->prepare("SELECT nom,photo,prix_depart,prix_reserve,description from objet where id=:id");
$requete2->bindParam(":id", $enchere[0]["id_objet"]);
$requete2->execute();
$objet = $requete2->fetchAll();
if(count($enchere)==0){
    echo "<p>pas d'enchere selectioner/p>";
}else{
    ?>
    <div class="card">

        <img src="<?php echo $objet[0]["photo"] ?>" alt="" class="card-img-top">
        <div class="card-body">
            <h5 class="card-title"> Nom : <?php echo htmlspecialchars($objet[0]["nom"]) ?></h5>
            <p class="card-text"> Description : <?php echo htmlspecialchars($objet[0]["description"]) ?></p>
            <p class="card-text"> Prix de départ : <?php echo htmlspecialchars($objet[0]["prix_depart"]) ?></p>
            <p class="card-text"> Prix de reserve : <?php echo htmlspecialchars($objet[0]["prix_reserve"]) ?></p>
        </div>
    </div>
    <?php
         if($MaxPrix[0]["MAX(prix)"]!= null){
    ?>
            <p class="card-text">Enchere la plus haute : <?php echo htmlspecialchars($MaxPrix[0]["MAX(prix)"])?></p>
             <form action="adjuger.php" method="post">
                 <input type="submit" class="btn btn-primary" value="Adjuger vendu !">
             </form>
    <?php
}
         else{echo"personne n'a encherie pour l'instant";}
    ?>
    <form action="annulerEnchere.php" method="post">
        <input type="submit" class="btn btn-primary" value="Annulez l'enchere">
    </form>

    <?php
}
?>

<?php