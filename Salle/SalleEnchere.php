<?php
require_once "../Header.php";
require_once "../Footer.php";

myHeader("Salle d'enchere");
require_once "../config.php";
$pdo = new PDO("mysql:host=".config::SERVER.";dbname=".config::BDD,config::USER,config::MDP);


$requeteEnchereEncours = $pdo->prepare("SELECT id_objet FROM encheresencours");
$requeteEnchereEncours->execute();
$EnchereEncours = $requeteEnchereEncours->fetchAll();

if(array_key_exists("idAcount",$_SESSION) && $_SESSION["idAcount"] != null){
    if(count($EnchereEncours)>0){
    $requetteObjet = $pdo->prepare("SELECT nom,description,prix_depart,photo FROM objet WHERE id=:id");
    $requetteObjet->bindParam("id",$EnchereEncours[0]["id_objet"]);
    $requetteObjet->execute();
    $objet = $requetteObjet->fetchAll();
    ?>

    <div>
        <img src="<?php echo $objet[0]["photo"] ?>" alt="" class="card-img-top"
        <div class="card-body">
            <h5 class="card-title"> Nom : <?php echo htmlspecialchars($objet[0]["nom"]) ?></h5>
            <p class="card-text"> Description : <?php echo htmlspecialchars($objet[0]["description"]) ?></p>
            <p class="card-text"> Prix de départ : <?php echo htmlspecialchars($objet[0]["prix_depart"]) ?></p>

        </div>
    </div>
    <div>
        <form action="encherir.php" method="post">
            <input name="prix" type="number" min="1">
            <input type="submit" value="Enchérir">
        </form>
    </div>
    <div id="enchere">

    </div>
    <script>const userId=<?php echo $_SESSION["idAcount"]?>;</script>
    <script src="verifEnchere.js"></script>
    <?php
    }else{
        ?>
        <script>alert("pas d'enchere pour le moment, retour a l'accueil");
                window.location.replace("../Accueil.php")
        </script>
        <?php
    }
}else{
    echo "Connectez Vous SVP";
}


?>

<?php
myFooter();
?>
