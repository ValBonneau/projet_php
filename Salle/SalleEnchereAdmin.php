<?php
require_once "../Header.php";
require_once "../Footer.php";

myHeader("Salle d'enchere");

?>
<div id="enCours">
    
</div>
<?php

require_once "../config.php";
$pdo = new PDO("mysql:host=".config::SERVER.";dbname=".config::BDD,config::USER,config::MDP);

$requeteEnchereEncours = $pdo->prepare("SELECT * FROM encheresencours");
$requeteEnchereEncours->execute();
$EnchereEncours = $requeteEnchereEncours->fetchAll();

$requeteEnchere = $pdo->prepare("SELECT nom,id FROM encheres");
$requeteEnchere->execute();
$Enchere = $requeteEnchere->fetchAll();
for($i = 0; $i <count($Enchere);$i++){

    echo '<h3 style="text-indent: 15px;">'.htmlspecialchars($Enchere[$i]["nom"]).' :</h3>';
    $requeteLot = $pdo->prepare("SELECT nom,id FROM lot where id_encheres=:id");
    $requeteLot->bindParam(":id",$Enchere[$i]["id"]);
    $requeteLot->execute();
    $lot = $requeteLot->fetchAll();
    for($j = 0; $j <count($lot);$j++){
        echo '<h4 style="text-indent: 30px;"> - '.htmlspecialchars($lot[$j]["nom"]).' :</h4>';
        $requeteObjet = $pdo->prepare("SELECT nom,id FROM objet where id_lot=:id");
        $requeteObjet->bindParam(":id",$lot[$j]["id"]);
        $requeteObjet->execute();
        $objet = $requeteObjet->fetchAll();
        for($k = 0; $k <count($objet);$k++){
            ?>
            <form action="SelectEnchere.php" method="post">
                <?php
                if(count($EnchereEncours)>0 && $EnchereEncours[0]["id_objet"]==$objet[$k]["id"]){
                ?>
                    <b><label style="text-indent: 60px;"><?php echo htmlspecialchars($objet[$k]["nom"]) ?></label></b>
                <?php
                }else{
                ?>
                    <label style="text-indent: 60px;"><?php echo htmlspecialchars($objet[$k]["nom"]) ?></label>
                <?php
                }
                ?>
                <input type="hidden" name="id" value="<?php echo htmlspecialchars($objet[$k]["id"]) ?>">
                <?php
                $requeteObjetV = $pdo->prepare("SELECT id_objet FROM objetv where id_objet=:id");
                $requeteObjetV->bindParam(":id",$objet[$k]["id"]);
                $requeteObjetV->execute();
                $ObjetV = $requeteObjetV->fetchAll();
                    if(count($ObjetV)==0){
                        echo '<input type="submit" class="btn btn-primary" value="Selectioner">';
                    }
                ?>
            </form>
            <?php

        }
    }
}

if(count($EnchereEncours)>0){
?>
    <script src="showPriceEnchere.js"></script>
<?php
}
myFooter();
?>