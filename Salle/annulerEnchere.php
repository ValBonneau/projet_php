<?php
require_once "../config.php";
$pdo = new PDO("mysql:host=".config::SERVER.";dbname=".config::BDD,config::USER,config::MDP);

$deleteEnchereEncours = $pdo->prepare("DELETE FROM encheresencours");
$deleteEnchereEncours->execute();

header("location: SalleEnchereAdmin.php");