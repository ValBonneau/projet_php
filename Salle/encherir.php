<?php
$prix = filter_input(INPUT_POST, "prix");
session_start();

require_once "../config.php";
$pdo = new PDO("mysql:host=".config::SERVER.";dbname=".config::BDD,config::USER,config::MDP);

$requeteEnchereEncours = $pdo->prepare("SELECT * FROM encheresencours");
$requeteEnchereEncours->execute();
$EnchereEncours = $requeteEnchereEncours->fetchAll();

if(array_key_exists("idAcount",$_SESSION) && $_SESSION["idAcount"] != null){
    $requetteMonPrix = $pdo->prepare("SELECT * FROM prixutilisateurencours where id_user = :id_user");
    $requetteMonPrix->bindParam(":id_user",$_SESSION["idAcount"]);
    $requetteMonPrix->execute();
    $MonPrix=$requetteMonPrix->fetchAll();
    if(count($MonPrix)==0){
        $requetteCreateMonPrix = $pdo->prepare("INSERT INTO prixutilisateurencours (id_encheres_encours,id_user,prix) values (:id_encheres,:id_user,:prix)");
        $requetteCreateMonPrix->bindParam(":id_user",$_SESSION["idAcount"]);
        $requetteCreateMonPrix->bindParam(":prix",$prix);
        $requetteCreateMonPrix->bindParam(":id_encheres", $EnchereEncours[0]["id"]);
        $requetteCreateMonPrix->execute();
    }else{
        $requetteChangeMonPrix = $pdo->prepare("UPDATE prixutilisateurencours SET prix=:prix where id_user=:id_user ");
        $requetteChangeMonPrix->bindParam(":prix",$prix);
        $requetteChangeMonPrix->bindParam(":id_user", $_SESSION["idAcount"]);
        $requetteChangeMonPrix->execute();
    }
}

header("location: SalleEnchere.php");