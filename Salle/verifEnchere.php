<?php
$userId=filter_input(INPUT_GET,"userId");


require_once "../config.php";
$pdo = new PDO("mysql:host=".config::SERVER.";dbname=".config::BDD,config::USER,config::MDP);

$requeteEnchere = $pdo->prepare("SELECT * FROM encheresencours");
$requeteEnchere->execute();
$Enchere = $requeteEnchere->fetchAll();

if(count($Enchere)==0){
    header("location: SalleEnchere.php");
}else {


    $requeteMaxPrix = $pdo->prepare("SELECT MAX(prix) FROM prixutilisateurencours WHERE id_encheres_encours=:id");
    $requeteMaxPrix->bindParam(":id", $Enchere[0]["id"]);
    $requeteMaxPrix->execute();
    $MaxPrix = $requeteMaxPrix->fetchAll();

    $requeteUserPrix = $pdo->prepare("SELECT prix FROM prixutilisateurencours WHERE id_encheres_encours=:id AND id_user=:idUser");
    $requeteUserPrix->bindParam(":id", $Enchere[0]["id"]);
    $requeteUserPrix->bindParam(":idUser", $userId);
    $requeteUserPrix->execute();
    $UserPrix = $requeteUserPrix->fetchAll();

    if (count($UserPrix) > 0) {
        if ($UserPrix[0]["prix"] >= $MaxPrix[0]["MAX(prix)"]) {
            echo '<p><span class="btn btn-success">vous gagnez la vente pour l&rsquo;instant</span> </p>';
        } else {
            echo '<p> <span class="btn btn-danger">vous perdez la vente pour l&rsquo;instant</span> </p>';

        }

    } else {
        echo '<p> <span class="btn btn-info">vous n&rsquo;avez pas encore encheri pour l&rsquo;instant</span> </p>';
    }
}