<?php
require_once "../config.php";
$pdo = new PDO("mysql:host=".config::SERVER.";dbname=".config::BDD,config::USER,config::MDP);

$requetteEnchereEncours = $pdo->prepare("SELECT * FROM encheresencours");
$requetteEnchereEncours->execute();
$EnchereEncours = $requetteEnchereEncours->fetchAll();

$requettePrixUtilisateur = $pdo->prepare("SELECT id_user,prix FROM prixutilisateurencours WHERE id_encheres_encours=:id ORDER BY prix DESC LIMIT 1");
$requettePrixUtilisateur->bindParam(":id",$EnchereEncours[0]["id"]);
$requettePrixUtilisateur->execute();
$PrixUtilisateur = $requettePrixUtilisateur->fetchAll();
var_dump($PrixUtilisateur);
echo "<br><br>";
$requettePrixUtilisateur->debugDumpParams();

$requetteObjetV = $pdo->prepare("INSERT INTO objetv (id_objet, id_user, date, prix) VALUES (:id_objet, :id_user , NOW(), :prix)");
$requetteObjetV->bindParam(":id_objet",$EnchereEncours[0]["id_objet"]);
$requetteObjetV->bindParam(":id_user",$PrixUtilisateur[0]["id_user"]);
$requetteObjetV->bindParam(":prix",$PrixUtilisateur[0]["MAX(prix)"]);
$requetteObjetV->execute();

echo "<br><br>";
$requetteObjetV->debugDumpParams();

$requetteDeleteEnchereEncours = $pdo->prepare("DELETE FROM encheresencours");
$requetteDeleteEnchereEncours->execute();



header("location: SalleEnchereAdmin.php");