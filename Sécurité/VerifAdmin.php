<?php
session_start();
if(array_key_exists("idAcount",$_SESSION) && $_SESSION["idAcount"] != null){
    require_once "../config.php";
    $pdo = new PDO("mysql:host=".Config::SERVER.";dbname=".Config::BDD,Config::USER,Config::MDP);
    $requete=$pdo->prepare("SELECT isAdmin FROM user where id=:id");
    $requete->bindParam(":id");
    $requete->execute();
    $isAdmin = $requete->fetchAll();
    if($isAdmin[0]["isAdmin"] != 1){
        header("location: WARN.php");
    }
}
else{
    header("location: ..\Connexion.php");
}
