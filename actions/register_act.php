<?php
var_dump($_POST);
$pseudo = filter_input(INPUT_POST,"pseudo");
$mail = filter_input(INPUT_POST,"mail");
$password = filter_input(INPUT_POST,"password");
$firstname = filter_input(INPUT_POST,"firstname");
$lastname = filter_input(INPUT_POST,"lastname");
$dateN = date('Y-m-d',strtotime( filter_input(INPUT_POST,"dateN")));

$password = password_hash($password,PASSWORD_BCRYPT);


$adresseP = filter_input(INPUT_POST,"adresseP");
require_once "../config.php";
$pdo = new PDO("mysql:host=".Config::SERVER.";dbname=".Config::BDD,Config::USER,Config::MDP);

$verif = $pdo->prepare('SELECT mail from user where mail=:mail');
$verif->bindParam(":mail",$mail);
$verif->execute();
$nbmail = $verif->fetchAll();
if(count($nbmail)==0) {
    $requete = $pdo->prepare('INSERT INTO user (pseudo,password,mail,nom,prenom,dateN,adresseP,isAdmin) values (:pseudo,:password,:mail,:nom,:prenom,:dateN,:adresseP,0)');
    $requete->bindParam(":pseudo", $pseudo);
    $requete->bindParam(":password", $password);
    $requete->bindParam(":mail", $mail);
    $requete->bindParam(":nom", $firstname);
    $requete->bindParam(":prenom", $lastname);
    $requete->bindParam(":dateN", $dateN);
    $requete->bindParam(":adresseP", $adresseP);
    $requete->execute();
    $error = $requete->errorInfo();
    echo "<br>";
    var_dump($error);


    header("location:../Accueil.php");
}
else{
    header("location:../Register.php?adressePrise=true");
}