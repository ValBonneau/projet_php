<?php
require_once "Header.php";
myHeader("Mon compte");
if (array_key_exists("idAcount",$_SESSION) && $_SESSION["idAcount"] != null) {
    require_once "config.php";
    $pdo = new PDO("mysql:host=" . config::SERVER . ";dbname=" . config::BDD, config::USER, config::MDP);
    $requete = $pdo->prepare("SELECT * from user where id=:id");
    $requete->bindParam(":id", $_SESSION["idAcount"]);
    $requete->execute();
    $lignes = $requete->fetchAll();

    ?>

    <form action="actions/updateAcount_act.php" method="post" style="margin:5% 20%;">
        <div class="form-group">
            <label for="">Pseudo</label>
            <input type="text" name="pseudo" class="form-control" value="<?php echo htmlspecialchars($lignes[0]["pseudo"]) ?>"required>
        </div>
        <div class="form-group">
            <label for="">Adresse Email</label>
            <input type="email" name="mail" class="form-control" value="<?php echo htmlspecialchars($lignes[0]["mail"]) ?>" required>
        </div>
        <div class="form-group">
            <label for="">Nom et Prénom</label>
            <div class="input-group">
                <input type="text" value="<?php echo htmlspecialchars($lignes[0]["nom"]) ?>" aria-label="First name" name="nom" class="form-control" required>
                <input type="text" value="<?php echo htmlspecialchars($lignes[0]["prenom"]) ?>" aria-label="Last name" name="prenom" class="form-control" required>
            </div>
        </div>
        <div class="form-group">
            <label for="">Date de Naîssance</label>
            <input type="date" class="form-control" value="<?php echo htmlspecialchars($lignes[0]["dateN"]) ?>" name="dateN" required>
        </div>
        <div class="form-group">
            <label for="">Adresse (complette)</label>
            <input type="text" value="<?php echo htmlspecialchars($lignes[0]["adresseP"]) ?>" class="form-control" rows="3" name="adresse">
        </div>
        <input type="hidden" value="<?php echo htmlspecialchars($_SESSION["idAcount"]) ?>" name="id">
        <input type="submit" value="Modifier" class="btn btn-primary">
    </form>
    <?php
}
require_once "Footer.php";
myFooter();
?>

